# Partie 0 : prolégomènes #

# Installation de quelques paquets utiles 
install.packages("tseries")
install.packages("fUnitRoots")
install.packages("forecast")

# Import des librairies
library(readr) 
library(ggplot2) 
library(dplyr) 
library(tseries) 

# Import des données
## Charger le fichier CSV sans en-têtes de colonnes
data <- read.csv("valeurs_mensuelles.csv", header = FALSE)

## Nommer les colonnes
colnames(data) <- c("Date", "Indice")

## Sélectionner uniquement la colonne des indices
indices <- data$Indice

## Créer un objet de série temporelle
serie_temporelle <- ts(indices, start = c(1990, 1), end = c(2019, 12), frequency = 12)

## L'affichage se fait sous forme de matrice (année, mois), très agréable à l'oeil.
print(serie_temporelle)

## Visualisation sous forme de graphe
plot(serie_temporelle, xlab = "Mois", ylab = "Indice", main = "Indice de production de produits pharmaceutiques.")

# Partie I #

# Premières analyses
## Au début ça faisait de la grosse daube.
### Et là Bibi se rend compte qu'une croissance exponentielle, ça se calme avec un log.
log_serie = log(serie_temporelle)
plot(
    log_serie, 
    main = "Log-indice de production de produits pharmaceutiques",
    xlab = "Mois", 
    ylab = "Log-indice")
acf(log_serie)

## Différentiation à l'ordre 1 et nouveaux tracés
### On fixe un lag maximal à 24 mois en guise de compromis entre complétion et simplicité.
log_serie_difference = diff(log_serie)
plot(
    log_serie_difference, 
    main = "Log-différence première de l'indice", 
    xlab = "Mois", 
    ylab = "Log-indice")
acf(log_serie_difference, main = "ACF de la log-différence première", 24)
pacf(log_serie_difference, main = "PACF de la log-différence première", 24)



# Tests de (non-)stationnarité
## Test ADF
adf_diff = adf.test(log_serie_difference, k = 7)
print(adf_diff)

## Test de PP
pp_diff = pp.test(log_serie_difference)
print(pp_diff)

## Test de KPSS
kpss_diff = kpss.test(log_serie_difference)
print(kpss_diff)

# Partie II #

# Comparaison des différents modèles ARMA(p,q)
## Comme on a assez peu de modèles à tester, c'est aussi rapide de le faire à la main.
### On est pas des sauvages, on trie par ordre lexicographique. Inverse. Bref.

arima(y, c(4,0,0))
BIC(arima(y, c(4,0,0)))

arima(y, c(3,0,0))
BIC(arima(y, c(3,0,0)))

arima(y, c(2,0,0))
BIC(arima(y, c(2,0,0)))

arima(y, c(1,0,0))
BIC(arima(y, c(1,0,0)))

arima(y, c(0,0,0))
BIC(arima(y, c(0,0,0)))



arima(y, c(4,0,1))
BIC(arima(y, c(4,0,1)))

arima(y, c(3,0,1))
BIC(arima(y, c(3,0,1)))

arima(y, c(2,0,1))
BIC(arima(y, c(2,0,1)))

arima(y, c(1,0,1))
BIC(arima(y, c(1,0,1)))

arima(y, c(0,0,1))
BIC(arima(y, c(0,0,1)))



# On a choisi notre candidat
modele_arima = arima(log_serie_difference, c(0,0,1))

# On veut tester la non-nullité du coefficient ma1

# Extraction des coefficients et des erreurs standard
coefs <- coef(modele_arima)
se <- sqrt(diag(vcov(modele_arima)))

# Coefficient et erreur standard pour ma1
coefficient_ma1 <- coefs["ma1"]
erreur_standard_ma1 <- se["ma1"]

# Nombre d'observations et de coefficients estimés
n_obs <- length(resid(modele_arima))
n_coefs <- length(coefs)

# Calcul de la statistique de test t
t_stat <- coefficient_ma1 / erreur_standard_ma1

# Calcul de la p-value associée, avec une approximation normale (359 - 2 = 357 >> 1)
p_value <- 2 * pnorm(-abs(t_stat))

# Affichage des résultats
cat("Statistique de test t:", t_stat, "\n")
cat("P-value associée:", p_value, "\n")

Statistique de test t: -16.83084 
P-value associée: 1.45042e-63 

# Le test est fortement concluant, ma1 est non nul.
# On tente un rapport de vraisemblance pour tester la non nullité jointe.

# Calcul du modèle restreint avec les coefficients fixés à zéro
modele_reduit <- arima(y, order = c(0, 0, 1), fixed = c(0, 0))
# Calcul du rapport de vraisemblance
LR_stat <- 2 * (logLik(modele_arima) - logLik(modele_reduit))

# Calcul de la p-value associée
p_value_LR <- 1 - pchisq(LR_stat, df = 2)

# Affichage des résultats
cat("Rapport de vraisemblance:", LR_stat, "\n")
cat("P-value associée:", p_value_LR, "\n")

cat("Rapport de vraisemblance:", LR_stat, "\n")
Rapport de vraisemblance: 125.8787 
cat("P-value associée:", p_value_LR, "\n")
P-value associée: 0 

# La conclusion est sans appel.

# On teste aussi la non nullité de la constante.

intercept_coef <- coefs["intercept"]
se_intercept <- se["intercept"]

t_stat_intercept <- intercept_coef / se_intercept
p_value_intercept <- 2 * pt(abs(t_stat_intercept), df = n_obs - n_coefs)
cat("P-value associée:", p_value_intercept, "\n")

P-value associée: 3.139217e-07 


# On passe à la blancheur, deux tests pour le prix d'un.

Box.test(modele_arima$residuals, type = "Ljung-Box")

Box.test(modele_arima$residuals, type = "Box-Pierce")


# Le QQplot pour voir à l'oeil la normalité
qqnorm(modele_arima$residuals)
qqline(modele_arima$residuals)

# Queues lourdes, on s'apprête à rejeter.

# Et les tests de normalité
shapiro.test(modele_arima$residuals)
jarque.bera.test(modele_arima$residuals)

# On passe à la prédiction

# Prédiction des deux prochaines valeurs avec le modèle ARIMA
predictions <- forecast(modele_arima, h = 2)

# Extraire les prédictions et les intervalles de confiance
valeurs_prédites <- predictions$mean
intervalle_inf <- predictions$lower[,2]  # Colonne pour le niveau de confiance 95%
intervalle_sup <- predictions$upper[,2]  # Colonne pour le niveau de confiance 95%

# Vos matrices de données
valeurs_prédites <- matrix(c(0.003875379, 0.003521628), ncol=2, dimnames=list(2020, c("Jan", "Feb")))
intervalle_inf <- matrix(c(-0.07171171, -0.08723756), ncol=2, dimnames=list(2020, c("Jan", "Feb")))
intervalle_sup <- matrix(c(0.07946247, 0.09428082), ncol=2, dimnames=list(2020, c("Jan", "Feb")))

# Convertir les matrices en data.frame
year_month <- expand.grid(year = rownames(valeurs_prédites), month = colnames(valeurs_prédites))


data <- data.frame(
  year = rep(rownames(valeurs_prédites), each = ncol(valeurs_prédites)),
  month = rep(colnames(valeurs_prédites), times = nrow(valeurs_prédites)),
  predicted = as.vector(valeurs_prédites),
  lower = as.vector(intervalle_inf),
  upper = as.vector(intervalle_sup)
  )


# Créer une colonne pour l'année-mois pour une meilleure visualisation
data$year_month <- paste(data$year, data$month, sep="-")

# Convertir 'month' en facteur avec les niveaux dans l'ordre souhaité
data$month <- factor(data$month, levels = c("Jan", "Feb"))

# Créer le plot avec ggplot2
ggplot(data, aes(x = year_month, y = predicted, color = month)) +
  geom_point() +
  geom_errorbar(aes(ymin = lower, ymax = upper), width = 0.2) +
  labs(x = "Année-Mois", y = "Valeur Prédite", color = "Mois") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))